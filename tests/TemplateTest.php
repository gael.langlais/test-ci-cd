<?php
require_once 'app/views/Template.php';

use PHPUnit\Framework\TestCase;
/**
 * @test
 * @covers Template
 **/
class TemplateTest extends TestCase
{
    protected $Template;
    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->Template = new Template();
    }

    /**
     * @test
     */
    public function displayPage()
    {
        $this->assertEquals("This is my page",$this->Template->displayPage());
    }
}
